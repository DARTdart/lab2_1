import 'dart:io';

int x = 0, y=0;
void prime_number(var a) {
  x = a ~/ 2;
  for (int i = 2; i <= x; i++) {
    if (a % i == 0) {
      print('$a is not a prime number');
      y = 1;
      break;
    }
  }

  if(y == 0){
    print('$a is a prime number');
  }
}

void main() {
  int? num;
  print("input number : ");
  num = int.parse(stdin.readLineSync()!);
  prime_number(num);
}